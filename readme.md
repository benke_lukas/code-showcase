# Dixons Code Showcase
## Description
This is a subsystem for verifying phone numbers. It can store a pair of Phone Number and Generated code which was sent to user using GSM. It can than verify this pair against input.
## Instalation
### Requirements
 - You need to have latest [Docker engine](https://docs.docker.com/install/linux/docker-ce/ubuntu/) installed
 - You need to have latest [docker-compose](https://docs.docker.com/compose/install/) installed
### Installing and running tests on Linux systems
There is a bash script for running common docker tasks prepared for your convenience.

 - clone the repository
 - make the ```laradock_control.sh``` executable
 - run ```mv laradock_dixons_code_showcase/env-example laradock_dixons_code_showcase/.env```
 - run ```./laradock_control.sh start```
 - run ```./laradock_control.sh ssh``` to SSH into docker workspace container
 - run ```composer install```
 - run ```phpunuit```
 
### Installing and running tests on other systems
 - copy *laradock_dixons_code_showcase/env-example* to *laradock_dixons_code_showcase/.env*
 - go into ```laradock_dixons_code_showcase```
 - run ```docker-compose up -d apache2 redis```
 - run ```docker-compose exec --user=laradock workspace bash```
 - run ```composer install```
 - run ```phpunuit```
