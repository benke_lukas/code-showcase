<?php

declare(strict_types=1);

namespace Domain\PhoneNumberVerification\Exceptions;


use Exception;
use Throwable;

class VerifyRequestTimeframeException extends Exception
{
    /**
     * @var int
     */
    private $timeframe;

    public function __construct(int $timeframe, Throwable $previous = null)
    {
        parent::__construct('', 0, $previous);
        $this->timeframe = $timeframe;
    }

    /**
     * @return int
     */
    public function getTimeframe(): int
    {
        return $this->timeframe;
    }
}
