<?php

declare(strict_types=1);

namespace Domain\PhoneNumberVerification\Exceptions;


use Exception;
use Throwable;

class VerifyRequestOverLimitException extends Exception
{
    /**
     * @var int
     */
    private $limit;
    /**
     * @var int
     */
    private $attemptsCount;

    public function __construct(int $attemptsCount, int $limit, Throwable $previous = null)
    {
        parent::__construct('', 0, $previous);

        $this->limit = $limit;
        $this->attemptsCount = $attemptsCount;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @return int
     */
    public function getAttemptsCount(): int
    {
        return $this->attemptsCount;
    }
}
