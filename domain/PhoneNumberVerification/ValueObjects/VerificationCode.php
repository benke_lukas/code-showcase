<?php

declare(strict_types=1);

namespace Domain\PhoneNumberVerification\ValueObjects;


use Illuminate\Support\Str;
use InvalidArgumentException;
use function sprintf;
use function strlen;

class VerificationCode
{
    /**
     * @var string
     */
    private $code;

    /**
     * @var bool
     */
    private $isVerified;

    public function __construct(string $code)
    {
        $this->validate($code);

        $this->code = $code;
    }

    /**
     * @param string $code
     * @throws InvalidArgumentException
     */
    private function validate(string $code): void
    {
        if (strlen($code) > 4) {
            throw new InvalidArgumentException(
                sprintf('Code must be %s long. Is %s long', 4, strlen($code))
            );
        }
        if ($code !== Str::upper($code)) {
            throw new InvalidArgumentException('Code must be all uppercase');
        }
    }

    public function equals(VerificationCode $code): bool
    {
        return $code->getCode() === $this->getCode();
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }
}
