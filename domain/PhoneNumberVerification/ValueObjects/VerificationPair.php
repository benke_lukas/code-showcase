<?php

declare(strict_types=1);

namespace Domain\PhoneNumberVerification\ValueObjects;


use Domain\Support\ValueObjects\PhoneNumber;

class VerificationPair
{
    /**
     * @var PhoneNumber
     */
    private $phoneNumber;
    /**
     * @var VerificationCode
     */
    private $code;

    public function __construct(PhoneNumber $phoneNumber, VerificationCode $code)
    {
        $this->phoneNumber = $phoneNumber;
        $this->code = $code;
    }

    /**
     * @return PhoneNumber
     */
    public function getPhoneNumber(): PhoneNumber
    {
        return $this->phoneNumber;
    }

    /**
     * @return VerificationCode
     */
    public function getCode(): VerificationCode
    {
        return $this->code;
    }
}
