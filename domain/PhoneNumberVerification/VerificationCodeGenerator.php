<?php

declare(strict_types=1);

namespace Domain\PhoneNumberVerification;


use Domain\PhoneNumberVerification\ValueObjects\VerificationCode;

interface VerificationCodeGenerator
{
    public function generate(): VerificationCode;
}
