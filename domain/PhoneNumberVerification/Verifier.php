<?php

declare(strict_types=1);

namespace Domain\PhoneNumberVerification;


use Domain\PhoneNumberVerification\ValueObjects\VerificationCode;
use Domain\PhoneNumberVerification\ValueObjects\VerificationPair;
use Domain\Support\ValueObjects\PhoneNumber;
use Infrastructure\PhoneNumberVerification\RedisVerifier;

interface Verifier
{
    public function generateAndStoreCodeForNumber(PhoneNumber $phoneNumber): VerificationCode;

    public function verifyCode(VerificationPair $verificationPair): bool;

    public function forgetPair(VerificationPair $verificationPair): bool;

    public function verifyCodeAndRemovePairOnSuccess(VerificationPair $verificationPair): bool;

    public function isPhoneNumberVerified(PhoneNumber $phoneNumber): bool;
}
