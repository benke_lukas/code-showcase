<?php

declare(strict_types=1);

namespace Domain\Support\ValueObjects;


use Illuminate\Support\Str;
use InvalidArgumentException;
use function preg_match;

class PhoneNumber
{
    private const PHONE_PATTERN = '#^(\+420)? ?[1-9][0-9]{2} ?[0-9]{3} ?[0-9]{3}$#';
    private const PREMIUM_PHONE_PATTERN = '#[0-9]{7}#';

    /**
     * @var string
     */
    private $phoneNumber;

    public function __construct(string $phoneNumber)
    {
        $this->validate($phoneNumber);

        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @param string $phoneNumber
     * @throws InvalidArgumentException
     */
    private function validate(string $phoneNumber): void
    {
        if (
            !preg_match(self::PHONE_PATTERN, $phoneNumber) &&
            !preg_match(self::PREMIUM_PHONE_PATTERN, $phoneNumber)
        ) {
            throw new InvalidArgumentException("Invalid Phone number pattern for number: '{$phoneNumber}'");
        }
    }

    public function __toString()
    {
        return $this->getPhoneNumber();
    }

    /**
     * @return string
     */
    public function getPhoneNumber(): string
    {
        return $this->phoneNumber;
    }

    public function getWithoutPrefix(): self
    {
        return new self(Str::replaceFirst('+420', '', $this->getPhoneNumber()));
    }
}
