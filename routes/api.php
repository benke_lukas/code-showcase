<?php

use App\Http\Controllers\API\V1\SendCodeForPhoneNumberVerification;
use App\Http\Controllers\API\V1\VerifyCodeFromPhoneNumberVerification;
use Dingo\Api\Routing\Router;

/**
 * @var Router $api
 */
$api->group(['prefix' => 'phoneNumberVerification'], function (Router $api) {
    $api->post('send', ['as' => 'api.front.phoneNumberVerification.send', 'uses' => SendCodeForPhoneNumberVerification::class]);
    $api->post('verify', ['as' => 'api.front.phoneNumberVerification.verify', 'uses' => VerifyCodeFromPhoneNumberVerification::class]);
});
