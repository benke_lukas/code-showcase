<?php

namespace Tests\Feature\Infrastructure\PhoneNumberVerification;

use Domain\PhoneNumberVerification\Exceptions\VerifyRequestTimeframeException;
use Domain\PhoneNumberVerification\ValueObjects\VerificationCode;
use Domain\PhoneNumberVerification\ValueObjects\VerificationPair;
use Domain\PhoneNumberVerification\Verifier;
use Domain\Support\ValueObjects\PhoneNumber;
use Illuminate\Redis\Connections\Connection;
use Infrastructure\PhoneNumberVerification\RandomVerificationCodeGenerator;
use Infrastructure\PhoneNumberVerification\RedisVerifier;
use Tests\TestCase;

class RedisVerifierTest extends TestCase
{
    /**
     * @var Connection
     */
    private $redisConnection;

    /**
     * @var Verifier
     */
    private $verifier;

    /**
     * @test
     */
    public function itCanGenerateAndStore(): void
    {
        $this->redisConnection->flushdb();
        $phoneNumber = new PhoneNumber('+420123456789');
        $code = $this->verifier->generateAndStoreCodeForNumber(
            $phoneNumber
        );

        /** @noinspection UnnecessaryAssertionInspection */
        $this->assertInstanceOf(VerificationCode::class, $code);
    }

    /**
     * @test
     */
    public function itThrowsExceptionIfMultipleRequestsForSameNumberInGivenTimeFrame(): void
    {
        $this->expectException(VerifyRequestTimeframeException::class);

        $phoneNumber = new PhoneNumber('+420123456789');
        $this->verifier->generateAndStoreCodeForNumber(
            $phoneNumber
        );

        $this->verifier->generateAndStoreCodeForNumber(
            $phoneNumber
        );
    }

    /**
     * @test
     */
    public function itDoesntThrowExceptionIfMultipleRequestsForSameNumberOutsideGivenTimeFrame(): void
    {
        $this->redisConnection->flushdb();

        $phoneNumber = new PhoneNumber('+420123456789');
        $this->verifier->generateAndStoreCodeForNumber(
            $phoneNumber
        );
        sleep(2);

        $this->verifier->generateAndStoreCodeForNumber(
            $phoneNumber
        );

        $this->assertTrue(true);
    }

    /**
     * @test
     */
    public function itCanVerifyCode(): void
    {
        $this->redisConnection->flushdb();

        $phoneNumber = new PhoneNumber('+420123456789');
        $code = $this->verifier->generateAndStoreCodeForNumber(
            $phoneNumber
        );

        $this->assertTrue(
            $this->verifier->verifyCode(
                new VerificationPair($phoneNumber, $code)
            )
        );
    }

    /**
     * @test
     */
    public function itCanForgetPair(): void
    {
        $this->redisConnection->flushdb();

        $phoneNumber = new PhoneNumber('+420123456789');
        $code = $this->verifier->generateAndStoreCodeForNumber(
            $phoneNumber
        );

        $this->assertTrue(
            $this->verifier->forgetPair(
                new VerificationPair($phoneNumber, $code)
            )
        );
    }

    /**
     * @test
     */
    public function itCanTellIfPhoneNumberIsVerified(): void {
        $this->redisConnection->flushdb();

        $phoneNumber = new PhoneNumber('+420123456789');
        $code = $this->verifier->generateAndStoreCodeForNumber(
            $phoneNumber
        );

        $this->assertFalse($this->verifier->isPhoneNumberVerified($phoneNumber));

        $this->verifier->verifyCode(
            new VerificationPair(
                $phoneNumber,
                $code
            )
        );

        $this->assertTrue($this->verifier->isPhoneNumberVerified($phoneNumber));
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->redisConnection = app('redis')->connection('test');

        $this->verifier = new RedisVerifier(
            new RandomVerificationCodeGenerator(),
            $this->redisConnection
        );
    }
}
