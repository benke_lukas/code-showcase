#!/usr/bin/env bash

SERVICES="apache2 redis"
LARADOCK_DIR="./laradock_dixons_code_showcase"

COMMAND=$1
ARGUMENTS=$2

function init() {
    if [ ! -d ${LARADOCK_DIR} ]; then
        git clone -b 'akon-v1.0' --single-branch --depth 1 git@gitlab.com:akoncrsro/laradock-acms.git $LARADOCK_DIR
    fi
}

function start() {
    local BUILD="0"

    if [ ! -z "$1" ]; then
        if [ "$1" = "build" ]; then
            BUILD="1"
        fi
    fi

    if [ "$BUILD" = "0" ]; then
        cd_into_laradock_dir &&  docker-compose up -d $SERVICES
    else
        cd_into_laradock_dir &&  docker-compose up -d --build $SERVICES
    fi
}
function stop() {
    cd_into_laradock_dir &&  docker-compose down
}
function restart() {
    start
    stop
}
function ssh() {
    local USER=$1

    if [ -z "$1" ]; then
        USER="laradock"
    fi

    local SERVICE="workspace"

    if [ -z "$2" ]; then
        SERVICE="workspace"
    else
        SERVICE="$2"
    fi

    cd_into_laradock_dir &&  docker-compose exec --user="$USER" "$SERVICE" bash
}

function cd_into_laradock_dir() {
    cd "$LARADOCK_DIR";
}

$COMMAND $ARGUMENTS
