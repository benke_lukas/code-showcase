<?php

declare(strict_types=1);

namespace Infrastructure\PhoneNumberVerification;


use Domain\PhoneNumberVerification\Exceptions\VerifyRequestOverLimitException;
use Domain\PhoneNumberVerification\Exceptions\VerifyRequestTimeframeException;
use Domain\PhoneNumberVerification\ValueObjects\VerificationCode;
use Domain\PhoneNumberVerification\ValueObjects\VerificationPair;
use Domain\PhoneNumberVerification\VerificationCodeGenerator;
use Domain\PhoneNumberVerification\Verifier;
use Domain\Support\ValueObjects\PhoneNumber;
use Illuminate\Redis\Connections\Connection as RedisConnection;
use Predis\Pipeline\Pipeline;
use function env;
use function time;

class RedisVerifier implements Verifier
{
    /**
     * @var VerificationCodeGenerator
     */
    private $codeGenerator;

    /**
     * @var RedisConnection
     */
    private $redis;

    public function __construct(
        VerificationCodeGenerator $codeGenerator,
        RedisConnection $redis
    )
    {
        $this->codeGenerator = $codeGenerator;
        $this->redis = $redis;
    }


    /**
     * @param PhoneNumber $phoneNumber
     * @return VerificationCode
     * @throws VerifyRequestTimeframeException
     * @throws VerifyRequestOverLimitException
     */
    public function generateAndStoreCodeForNumber(PhoneNumber $phoneNumber): VerificationCode
    {
        $code = $this->codeGenerator->generate();
        $pair = new VerificationPair($phoneNumber, $code);

        if ($this->redis->exists($this->getKey($phoneNumber)) !== '') {
            $lastGeneratedAt = (string)$this->redis->get($this->getGeneratedAtKey($phoneNumber));
            $count = (int)$this->redis->get($this->getCountKey($phoneNumber));
            $attemptsLimit = (int)env('PHONE_VERIFICATION_ATTEMPTS_LIMIT', 10);
            $timeframeLimit = (int)env('PHONE_VERIFICATION_TIMEFRAME_LIMIT', 60);

            $this->guardAgainstTooManyAttempts($count, $attemptsLimit);
            $this->guardAgainstTooManyAttemptsInTimeframe($lastGeneratedAt, $timeframeLimit);
        }

        $this->redis->pipeline(function (Pipeline $pipe) use ($phoneNumber, $pair) {
            $pipe->set(
                $this->getKey($phoneNumber),
                $pair->getCode()->getCode()
            );
            $pipe->set(
                $this->getGeneratedAtKey($phoneNumber),
                time()
            );
            $pipe->set(
                $this->getIsVerifiedKey($phoneNumber),
                false
            );
            $pipe->incr(
                $this->getCountKey($phoneNumber)
            );
            $pipe->expire(
                $this->getCountKey($phoneNumber),
                60 * 60 * 24
            );
        });

        return $pair->getCode();
    }

    private function getKey(PhoneNumber $phoneNumber): string
    {
        return "phoneNumberVerification:{$phoneNumber->getPhoneNumber()}";
    }

    public function isPhoneNumberVerified(PhoneNumber $phoneNumber): bool
    {
        return (bool)$this->redis->get(
            $this->getIsVerifiedKey($phoneNumber)
        );
    }

    public function verifyCodeAndRemovePairOnSuccess(VerificationPair $verificationPair): bool
    {
        $verified = $this->verifyCode($verificationPair);

        if ($verified) {
            $this->forgetPair($verificationPair);
        }

        return $verified;
    }

    public function verifyCode(VerificationPair $verificationPair): bool
    {
        $codeFromRedis = (string)$this->redis->get(
            $this->getKey($verificationPair->getPhoneNumber())
        );
        $code = new VerificationCode($codeFromRedis);

        $equals = $code->equals($verificationPair->getCode());
        if ($equals) {
            $this->redis->set(
                $this->getIsVerifiedKey($verificationPair->getPhoneNumber()),
                true
            );
        }
        return $equals;
    }

    public function forgetPair(VerificationPair $verificationPair): bool
    {
        $phoneNumber = $verificationPair->getPhoneNumber();
        $key = $this->getKey($phoneNumber);
        $this->redis->pipeline(function (Pipeline $pipe) use ($key, $phoneNumber) {
            $pipe->del([
                $key,
                $this->getIsVerifiedKey($phoneNumber),
                $this->getGeneratedAtKey($phoneNumber),
                $this->getCountKey($phoneNumber)
            ]);
        });

        return
            !$this->redis->exists($key) &&
            !$this->redis->exists($this->getIsVerifiedKey($phoneNumber)) &&
            !$this->redis->exists($this->getGeneratedAtKey($phoneNumber)) &&
            !$this->redis->exists($this->getCountKey($phoneNumber));
    }

    protected function getIsVerifiedKey(PhoneNumber $phoneNumber): string
    {
        return "{$this->getKey($phoneNumber)}:is_verified";
    }

    protected function getGeneratedAtKey(PhoneNumber $phoneNumber): string
    {
        return "{$this->getKey($phoneNumber)}:generated_at";
    }

    protected function getCountKey(PhoneNumber $phoneNumber): string
    {
        return "{$this->getKey($phoneNumber)}:count";
    }

    /**
     * @param int $count
     * @param int $attemptsLimit
     * @throws VerifyRequestOverLimitException
     */
    protected function guardAgainstTooManyAttempts(int $count, int $attemptsLimit): void
    {
        if ($count >= $attemptsLimit) {
            throw new VerifyRequestOverLimitException($count, $attemptsLimit);
        }
    }

    /**
     * @param string $lastGeneratedAt
     * @param int $timeframeLimit
     * @throws VerifyRequestTimeframeException
     */
    protected function guardAgainstTooManyAttemptsInTimeframe(string $lastGeneratedAt, int $timeframeLimit): void
    {
        if (
            ($lastGeneratedAt !== '') &&
            time() - $lastGeneratedAt <= $timeframeLimit
        ) {
            throw new VerifyRequestTimeframeException($timeframeLimit);
        }
    }
}
