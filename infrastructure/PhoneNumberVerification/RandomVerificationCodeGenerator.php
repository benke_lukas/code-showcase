<?php

declare(strict_types=1);

namespace Infrastructure\PhoneNumberVerification;


use Domain\PhoneNumberVerification\ValueObjects\VerificationCode;
use Domain\PhoneNumberVerification\VerificationCodeGenerator;
use Illuminate\Support\Str;

class RandomVerificationCodeGenerator implements VerificationCodeGenerator
{
    public function generate(): VerificationCode
    {
        $random = Str::random(4);
        $random = Str::upper($random);

        return new VerificationCode($random);
    }

}
