<?php

declare(strict_types=1);

namespace App\ValidationRules;


use Illuminate\Contracts\Validation\Rule;
use InvalidArgumentException;

class PhoneNumber implements Rule
{
    public function passes($attribute, $value)
    {
        try {
            new \Domain\Support\ValueObjects\PhoneNumber($value);

            return true;
        } catch (InvalidArgumentException $e) {
            return false;
        }
    }

    public function message()
    {
        return 'Pole :attribute není platné české číslo';
    }

}
