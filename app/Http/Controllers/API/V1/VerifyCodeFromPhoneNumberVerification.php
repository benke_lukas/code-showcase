<?php

declare(strict_types=1);

namespace App\Http\Controllers\API\V1;


use App\Http\Controllers\API\BaseController;
use App\Http\Requests\VerifyCodeRequest;
use Dingo\Api\Http\Response;
use Domain\PhoneNumberVerification\ValueObjects\VerificationCode;
use Domain\PhoneNumberVerification\ValueObjects\VerificationPair;
use Domain\PhoneNumberVerification\Verifier;
use Domain\Support\ValueObjects\PhoneNumber;

class VerifyCodeFromPhoneNumberVerification extends BaseController
{
    /**
     * @var Verifier
     */
    private $verifier;

    public function __construct(Verifier $verifier)
    {
        $this->verifier = $verifier;
    }

    public function __invoke(VerifyCodeRequest $request): Response
    {
        $verified = $this->verifier->verifyCode(
            new VerificationPair(
                new PhoneNumber($request->input('phoneNumber')),
                new VerificationCode(
                    $request->input('code')
                )
            )
        );

        if ($verified) {
            return $this->response->array([
                'verified' => true
            ]);
        } else {
            return $this->response->errorBadRequest('Wrong code or phone number');
        }
    }
}
