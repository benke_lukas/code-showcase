<?php

declare(strict_types=1);

namespace App\Http\Controllers\API\V1;


use App\Http\Controllers\API\BaseController;
use App\Http\Requests\SendCodeForPhoneNumberVerificationRequest;
use App\PhoneNumberVerification\CodeRequested;
use Dingo\Api\Http\Response;
use Domain\PhoneNumberVerification\Exceptions\VerifyRequestOverLimitException;
use Domain\PhoneNumberVerification\Exceptions\VerifyRequestTimeframeException;
use Domain\PhoneNumberVerification\Verifier;
use Domain\Support\ValueObjects\PhoneNumber;
use Illuminate\Notifications\ChannelManager;
use Illuminate\Support\Facades\Notification;

class SendCodeForPhoneNumberVerification extends BaseController
{
    /**
     * @var Verifier
     */
    private $verifier;

    public function __construct(
        Verifier $verifier
    )
    {
        $this->verifier = $verifier;
    }

    public function __invoke(SendCodeForPhoneNumberVerificationRequest $request): Response
    {
        try {
            $phoneNumber = new PhoneNumber($request->input('phoneNumber'));
            $code = $this->verifier->generateAndStoreCodeForNumber(
                $phoneNumber
            );

            Notification::route('telefonicaSMS', $phoneNumber->getPhoneNumber())
                ->notify(new CodeRequested($phoneNumber, $code));

            return $this->response->noContent();
        } catch (VerifyRequestOverLimitException $e) {
            return $this->response->error(
                "Zaslali jste více než {$e->getAttemptsCount()} požadavků za 24 hodin. Zkuste to znovu později.",
                429
            );
        } catch (VerifyRequestTimeframeException $e) {
            return $this->response->error(
                "Za {$e->getTimeframe()} můžete udělat pouze 1 požadavek",
                429
            );
        }

        return $this->response->errorInternal();
    }
}
