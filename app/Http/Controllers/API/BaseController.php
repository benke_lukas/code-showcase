<?php

declare(strict_types=1);

namespace App\Http\Controllers\API;

use Dingo\Api\Routing\Helpers;
use Domain\Users\Entities\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class BaseController
{
    use Helpers,
        AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;
}
