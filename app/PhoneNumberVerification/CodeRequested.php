<?php

namespace App\PhoneNumberVerification;

use Domain\PhoneNumberVerification\ValueObjects\VerificationCode;
use Domain\Support\ValueObjects\PhoneNumber;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Infrastructure\SMS\Message\Message;

class CodeRequested extends Notification
{
    use Queueable;
    /**
     * @var PhoneNumber
     */
    private $phoneNumber;
    /**
     * @var VerificationCode
     */
    private $verificationCode;

    /**
     * Create a new notification instance.
     *
     * @param PhoneNumber $phoneNumber
     * @param VerificationCode $verificationCode
     */
    public function __construct(
        PhoneNumber $phoneNumber,
        VerificationCode $verificationCode
    )
    {
        $this->phoneNumber = $phoneNumber;
        $this->verificationCode = $verificationCode;
    }

    /**
     * @return PhoneNumber
     */
    public function getPhoneNumber(): PhoneNumber
    {
        return $this->phoneNumber;
    }

    /**
     * @return VerificationCode
     */
    public function getVerificationCode(): VerificationCode
    {
        return $this->verificationCode;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['telefonicaSMS'];
    }

    public function toTelefonicaSMS(): Message
    {
        return new Message(
            $this->phoneNumber,
            '',
            "Vas kod pro overeni na webu InzertExpres.cz je: {$this->verificationCode->getCode()}"
        );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
