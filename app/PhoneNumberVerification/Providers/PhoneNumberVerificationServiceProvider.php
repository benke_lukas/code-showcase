<?php

namespace App\PhoneNumberVerification\Providers;

use Domain\PhoneNumberVerification\VerificationCodeGenerator;
use Domain\PhoneNumberVerification\Verifier;
use GuzzleHttp\Client;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\ServiceProvider;
use Infrastructure\PhoneNumberVerification\RandomVerificationCodeGenerator;
use Infrastructure\PhoneNumberVerification\RedisVerifier;
use Infrastructure\SMS\Channels\TelefonicaSMS;
use Infrastructure\SMS\Telefonica\BaID;
use function env;
use function storage_path;

class PhoneNumberVerificationServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(Verifier::class, function (Application $app) {
            return new RedisVerifier(
                new RandomVerificationCodeGenerator(),
                $app->make('redis')->connection('phoneNumberVerification')
            );
        });
        $this->app->bind(VerificationCodeGenerator::class, RandomVerificationCodeGenerator::class);

        $this->app->bind(\Infrastructure\SMS\Telefonica\API\Client::class, function (Application $app) {
            return new \Infrastructure\SMS\Telefonica\API\Client(
                new Client([
                    'base_uri' => 'https://smsconnector.cz.o2.com',
                    'cert' => [
                        storage_path(env('TELEFONICA_CERT_FILE')),
                        env('TELEFONICA_CERT_PASSWORD')
                    ],
                    'ssl_key' => [
                        storage_path(env('TELEFONICA_KEY_FILE')),
                        env('TELEFONICA_CERT_PASSWORD')
                    ]
                ]),
                new BaID(env('TELEFONICA_BA_ID'))
            );
        });

        Notification::extend('telefonicaSMS', function (Application $app) {
            return new TelefonicaSMS(
                $app->make(\Infrastructure\SMS\Telefonica\API\Client::class)
            );
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
